(function () {
    "use strict";
    angular.module("RegApp").controller("RegCtrl", RegCtrl);

    RegCtrl.$inject = ["$http", "$scope"];
    
    function RegCtrl($http, $scope) {
        var regCtrlself = this;
        
                
        regCtrlself.onSubmit = onSubmit;
        regCtrlself.initForm = initForm;
        regCtrlself.onReset = onReset;
        regCtrlself.emailFormat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
        regCtrlself.passwordFormat = /^(?=.*\d)(?=.*[@#$])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
        regCtrlself.contactNumberFormat = /^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]*$/;
                
        regCtrlself.user ={
                        }
        
        regCtrlself.countries =[
                    {name: "Please select", value: "0"},
                    {name: "Singaporean", value: "1"},
                    {name: "Malaysian", value: "2"},
                    {name: "Indonesian", value: "3"},
                    {name: "Thai", value: "4"},
                    {name: "Others", value: "5"}
                ];
            
        function initForm() {
                regCtrlself.user.selectCountry="0";
                // this regCtrlself.... is to tell the function when they init form to auto show value 0
                regCtrlself.user.gender = "F";
                // this is to let the form be loaded with Female being checked and the span will not be activated. what is written in html is only at user interface.
                }
            
        
            
        function onReset(){
                regCtrlself.user = Object.assign({}, regCtrlself.user);
                regCtrlself.registrationform.$setPristine();
                regCtrlself.registrationform.$setUntouched();
            }    
            
        function onSubmit() {
                return ("Form Submitted");
                console.log(regCtrlself.user.email);
                console.log(regCtrlself.user.password);
                console.log(regCtrlself.user.name);
                console.log(regCtrlself.user.gender);
                console.log(regCtrlself.user.dob);
                console.log(regCtrlself.user.minAge);
                console.log(regCtrlself.user.selectCountry);
                console.log(regCtrlself.user.contactNumber);
                $http.post("/api/submitForm",regCtrlself.user).then((result)=>{
                        console.log("result>" + result);
                        var registeredUser = result.data;
                        console.log("email>" + registeredUser.email);
                        console.log("password>" + registeredUser.password);
                        console.log("name>" + registeredUser.name);
                        console.log("Gender>" + registeredUser.gender);
                        console.log("Date of birth>" + registeredUser.dob);
                        console.log("Nationality>" + registeredUser.selectCountry);
                        console.log("Contact Number>" + registeredUser.contactNumber);
                                
                    }).catch((error)=>{
                        console.log("error> " + error);             
                    // ng-module for all items in index.html, post all items in the server side
                    })
                
                    //self.isSubmit = "Thank you";
                    
            regCtrlself.initForm();
    }
    
    }
})();