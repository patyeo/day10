set up new directory, gitbucket and install express, bootstrap, font-awesome, angular, etc

in index.html, add on
<!DOCTYPE html>
<html lang="en" ng-app="RegApp">tag this to turn this page into an angular app
    
    <head>
        <meta charset="UTF-8">
        <title>Registration</title>
        <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css" />
        <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap-theme.css" />
        <link rel="stylesheet" href="/css/main.css" />
    </head>
    
    <body> controller can be place in body as well
    <div class="col-sm-2 col-lg-4"></div>
        <div ng-controller="RegCtrl as ctrl" class="col-sm-8 col-lg-4 registration-page"> so that the controller can be activated
            <h1> Registration </h1>
            <form name="registrationform" novalidate> </form>

and before end of body
<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/bower_components/angular/angular.min.js"></script>
<script src="js/app.js"></script> - use the name of the js in the client folder

go regexr.com to find and create regular expression to be used.

In app.module.js, must start with 
(function () {
    "use strict";
    angular.module("RegApp", []);  
})();
** RegApp is the name of the App created for the project to match with app.controller
name of directives to be added in the []. to add more, just add comma within the []

In app.controller.js, must start with
(function () {
    "use strict";
    angular.module("RegApp").controller("RegCtrl", RegCtrl);

    RegCtrl.$inject = ["$http"];
    
    function RegCtrl($http) {
** functions to be within this function: specific functions in the controller.js file needed by the app**
    };
})();

Create end point (the back end) for the data to be posted to at server js file which looks like this
app.post("/api/xxx",(req,res)=>{
    console.log("submit xxx");
    console.log(req.body);
    res.status(200).json(xxx);
});

ensure that the end point $http.post("/api/submitQuizes",self.finalAnswer).then((result)=>{
                    self.isCorrect = result.data;
                }).catch((e)=>{
                    console.log(e);
in /client/controller.js match server/xxx.js so that they can connect.

Create a xxx.json file under server to put questions for the quiz
in quizes.json (json file cannot comment)
// most friendly data format for javascript
// need to double quote the var (key)
// answers are in array form and need to iterate to a format that can be read
